##스탭1##
1. DatabaseConfig.java 설정 이해하기
- @MapperScan 에 의한 동작 이해
- mybatis-config.xml 안에 settings, typeAliases 설정 이해
- service(java generic type) 와 serviceImpl(implements) 사용법 이해 및 service 인터페이스를 구현하는 이유 이해
- controller와 service 사용법 이해

##스탭2##
- spring security 사용법 이해
- thymeleaf 설정 및 사용법 이해
- WebConfiguration 설정
1. 리소스 static 설정 이해
2. 타임리프 설정 이해
3. addFormatters, configureMessageConverters 가 기본설정만 되어있지만 
이게 custom 설정으로 어떻게 사용되는지 구글링을 통해 이해하기

##스탭3##
1. thymeleaf 기능 따라하기
2. thymeleaf 에서의 security 기능 이해 및 사용법 익히기
3. thymeleaf 를 이용하여 화면에 목록 뿌리기 개발 - 직접하기
4. 권한에 따른 버튼 및 메뉴 접근 설정 해보기 - 직접하기
- 게시판관리 : 사용자 관리자 모두 사용가능
- 공지사항 관리 : 사용자 보기만 가능, 관리자 모두 가능
- 사용자 관리 : 관리자만 메뉴를 보고 접근 가능
5. 부트스트랩 모달기능 적용해보기 - 직접하기

##스탭4##
1. board.html
- 본인이 개발한 내용과 비교하여 다른 개발방식에 대한 이해
- 스크립트에 있는 doDelete, doAllDelete 기능 구현
- 목록에 있는 수정일 년-월-일 시:분:초 로 표현하기 값 없으면 빈값으로 표현
- 목록에 있는 제목 최대 10글자만 보이게 하고 10글자 초과시 ...으로 표시하기 -> 날짜표기방식과 동일한 방식으로 view
단에서 처리 가능하게 구현할 것

2. board(게시판)에 있는 페이징 기능 이해하기
- 페이지 넘길때마다 검색 값이 유지되는 이유 설명
- 페이지 넘길떄마다 검색 값이 유지되고 다음 데이타가 나오는 이유 설명
- 순번표시가 페이지 넘길떄마다 자기 페이지가 아닌 전체 페이지에서 차감 카운트되어서 표시되는 이유 설명

3. admin02.html -> notice.html 이름 바꾸고 기능구현하기
- 목록 + 페이징 기능구현
- 게시판과 동일하게 등록, 수정, 삭제, 선택삭제 기능 구현

4. admin03.html -> member.html 로 이름 바꾸고 기능구현하기
- 목록 + 페이징 기능 구현
- 게시판과 동일하게 등록, 수정, 삭제, 선택삭제 기능 구현
- 계정 등급 컬럼 추가 ( MariaDB )
컬럼생성정보는 아래와 같이 입력하여 생성
name : level
type : int
default : 0
comment : 0: admin, 1: user

# MVC 패턴에 의해 정말 특수한 경우를 제외하고는 원데이타를 컨트롤러가 VIEW에게 제공해주고 다양한 포맷 지원은 VIEW
에서 하는게 좋음

##스탭5##
1. 본인이 구현한것과 비교하여 다른방법으로도 구현이 가능함을 이해
2. 폼 체크 및 컨트롤러에서 체크하는 방식을 보고 어떤 예외상황을 고려하여 코딩한것인지 아는것이 중요
#추가개발
1. 작성글은 관리자에게는 모든 권한을 사용자인 경우에는 작성자 본인만 컨트롤 할 수 있어야 한다.
2. step5 구조를 보고 공지사항만 다시 개발을 해본다 이번에는 실제 상용서비스에 제공해준다 생각하고 다양한상황에 맞게 오류가
발생하는 케이스를 이해하고 예외처리해준다.
3. 공지사항 기능 정의
- 사용자는 onoff : Y 인것만 보이게 처리
- 관리자는 모든 목록을 볼 수 있다.
- 사용자는 등록, 수정, 삭제 권한이 없다. 오직 관리자만 권한이 있다.
- 검색기능 : 제목, 내용, onoff
- 목록에 보여줄 내용 : 체크박스, 순번, 제목(10글자), 내용(20글자), Action
  > Action 컬럼은 관리자만 보이게 해 줄 것, 사용자는 Action 타이틀도 안보여야함
- 최근 등록 게시물이 먼저 보이게 처리
4. 난이도 상급과제 - 사용자 테이블에 name 컬럼추가한다. -> 사용자관리화면에 기능 추가
- 로그인 시 xxx님 반갑습니다에 이름 표시
- 게시판 등록시 작성자는 id 로 저장되어야 함
- 해당  기능은 spring security 에서 해결되어야 함, 각 컨트롤러에서 디비조회해서 가져오는건 X

##스탭6##
AuthProvider 의 기능 분리
spring boot security 가 제공해준 기능 최대한 활용하기
1. AuthProvider 에 들어간 기능들은 spring boot security 가 제공해준 기능중 일부만 사용했음
2. 프로바이더에 들어간 일부 기능들을 스프링에서 제공해주는 UserDetailsService 를 이용하여 분리한다.
3. 위 기능을 사용하게 되면 스프링에서 제공해주는 UserDetails 도 사용해야한다.
4. 해당 옵션은 나중에 사이트 권한 복잡성 추가시 언젠가는 사용하게 된다.

##스탭7##
log4j 활용
1. log4j 를 이용하여 서비스별 로그 생성
2. 자동 백업 설정

# /board, /member, /notice 주소 접속 시
welcome to board / member / notice 를 콘솔에 각각페이지 맞는 이름으로 출력
로그파일 : /home/testboard/log/my-webapp.log
하루지나면 /home/testboard/log/bak/%d{yyyy-MM}/%d{yyyy-MM-dd}.log 형태로 빽업파일생성

# 서비스 별 로그파일
- 사용자관리 조회시
/home/testboard/log/member/member-webapp.log 형태로 생성
하루지나면
/home/testboard/log/member/bak/%d{yyyy-MM-dd}/%d{yyyy-MM-dd}.log
형태로 빽업파일생성

- 게시판 조회시
/home/testboard/log/board/board-webapp.log 형태로 생성
하루지나면
/home/testboard/log/board/bak/%d{yyyy-MM-dd}/%d{yyyy-MM-dd}.log 형태로
빽업파일생성

로그파일정보는 아래와 같이 생성
userid=로그인아이디 message=검색파라미터정보

##스탭8##
해킹 방지 및 보안강화를 해라
1. 게시판 VIEW에 수정버튼은 모두에게 보이게 한다 ( 해킹 된것처럼 권한없을때 못보는 버튼도 다 보이게 해준다. )
2. 게시판 수정 시 작성자=로그인 같을때와 ADMIN 일때만 Controller 에 접근가능하게 하고 그 외에는 접근을 막아라.
- 인터셉터 사용금지
3. 여러가지 방법이 있지만 컨트롤러에서 막을 수 있는 방법이 가장 안전하다.


