package com.nautestech.www.model;

import lombok.Data;

@Data
public class Board {
	public String seq;
	public String title;
	public String contents;
	public String regdate;
}
