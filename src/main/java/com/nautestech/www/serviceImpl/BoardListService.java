package com.nautestech.www.serviceImpl;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nautestech.www.mapper.BoardMapper;
import com.nautestech.www.model.Board;
import com.nautestech.www.service.insert;
import com.nautestech.www.service.list;

@Service
public class BoardListService implements list<Board>, insert {
	
	@Autowired
	BoardMapper mapper;

	@Override
	public List<Board> getList(HashMap<String, Object> param) {
		return mapper.getList(param);
	}

	@Override
	public void setInsert(HashMap<String, Object> param) {
		mapper.setInsert(param);
	}

}
